Handtracking
============

About
-----

 A simple handtracking test script using `mediapipe` and `opencv2`.


Sources
-------

 - <https://www.youtube.com/watch?v=NZde8Xt78Iw>


Ideas
-----

 - Blender hand animation
 - Keyboard hand movement analytics
