#!/usr/bin/env python3

"""
References
----------

 - <https://www.youtube.com/watch?v=NZde8Xt78Iw>


Requirements
------------

 - `opencv`
 - `mediapipe`
"""

import cv2
import mediapipe as mp
import time


cap = cv2.VideoCapture("/dev/video0")
# cap.set(3, 1920)  # Set input width
# cap.set(4, 1080)  # Set input height

mpHands = mp.solutions.hands
# hands = mpHands.Hands()
hands = mpHands.Hands(True, 2, 0.5, 0.5)
mpDraw = mp.solutions.drawing_utils

pTime = 0  # previous time
cTime = 0  # current time


while True:
    success, img = cap.read()

    # Process images (Hand detection)
    imgRGB = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)  # Convert Colors
    results = hands.process(imgRGB)                # Processing the image with `mediapipe.solutions.hands.Hands()`
    # print(results.multi_hand_landmarks)  #TEST

    if results.multi_hand_landmarks:
        for handLms in results.multi_hand_landmarks:
            # Paints the handmarks and connections into the image

            for id, lm in enumerate(handLms.landmark):
                # print(id, lm)  #TEST: print the landmark values

                # Calculate pixel coordinates from landmark positions
                h, w, c = img.shape  # get image coordinates
                cx, cy = int(lm.x * w), int(lm.y * h)
                print(id, cx, cy)

                # Highlight specific landmarks (using the calculated pixel coordinates)
                if id == 0:  # Hand root
                    # cv2.circle(<image>, <positions>, <size>, <color>, <style>)
                    cv2.circle(img, (cx, cy), 15, (255, 0, 255), cv2.FILLED)

                if id == 4:  # Thumb tip
                    cv2.circle(img, (cx, cy), 10, (255, 0, 0), cv2.FILLED)

                if id == 8:  # Pointer finger tip
                    cv2.circle(img, (cx, cy), 10, (255, 255, 0), cv2.FILLED)


            # mpDraw.draw_landmarks(img, handLms)
            mpDraw.draw_landmarks(img, handLms, mpHands.HAND_CONNECTIONS)

    # Calculate FPS
    cTime = time.time()
    fps = 1 / (cTime - pTime)
    pTime = cTime
    cv2.putText(img, str(int(fps)), (10, 70), cv2.FONT_HERSHEY_PLAIN, 3, (255, 0, 255), 3)

    # Show images
    cv2.imshow("image", img)
    if cv2.waitKey(1) & 0xFF == ord('x'):  # When pressing 'x' break the loop
        break


# Clean up for exit
cap.release()
cv2.destroyAllWindows
